# -*- coding: utf-8 -*-
import pycurl
from StringIO import StringIO


class WebAgentSim:
    def __init__(self):
        self.curl = pycurl.Curl()

    def get_page(self, url, postfields, *args, **kargs):

        self.curl.setopt(self.curl.URL, url)
        self.curl.setopt(self.curl.POSTFIELDS, postfields)
        self.curl.bodyio = StringIO()
        self.curl.setopt(self.curl.WRITEFUNCTION, self.curl.bodyio.write)
        self.curl.get_body = self.curl.bodyio.getvalue
        self.curl.headio = StringIO()
        self.curl.setopt(self.curl.HEADERFUNCTION, self.curl.headio.write)
        self.curl.get_head = self.curl.headio.getvalue

        self.curl.setopt(pycurl.FOLLOWLOCATION, 1)
        self.curl.setopt(self.curl.POST, 1)

        self.curl.setopt(self.curl.CONNECTTIMEOUT, 60)
        self.curl.setopt(self.curl.TIMEOUT, 120)
        self.curl.setopt(self.curl.NOSIGNAL, 1)
        self.curl.setopt(pycurl.USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36')
        self.curl.setopt(self.curl.COOKIELIST, "JSESSIONID")


        self.curl.perform()
        if self.curl.getinfo(pycurl.HTTP_CODE) != 200:
            raise Exception('HTTP code is %s' % self.curl.getinfo(pycurl.HTTP_CODE))
    
        return self.curl.get_body()

    def get_file(self, url):
        fp = open('captcha', "wb")
        self.curl.reset()
        self.curl.setopt(self.curl.CONNECTTIMEOUT, 60)
        self.curl.setopt(self.curl.TIMEOUT, 120)
        self.curl.setopt(self.curl.NOSIGNAL, 1)
        self.curl.setopt(self.curl.URL, url)

        self.curl.setopt(self.curl.WRITEDATA, fp)
        self.curl.setopt(self.curl.COOKIELIST, "JSESSIONID")

        self.curl.perform()

        fp.close()
        with open('captcha', 'r') as content_file:
            content_file.seek(0)
            content = content_file.read()
        return content

